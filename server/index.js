const express = require("express");
const app = express();
app.use(express.json());

const cookieParser = require("cookie-parser");
app.use(cookieParser("secret"));
const expressSession = require("express-session");
var cors = require('cors');
const http = require('http').Server(app);
const io = require('socket.io')(http, { cors: { origin: 'http://192.168.0.12:6001', methods: ["GET", "POST"] } })
app.use(cors({ credentials: true, origin: 'http://192.168.0.12:6001' }));

app.use(expressSession({
    secret: "nasjcklna",
    resave: false,
    saveUninitialized: false
}))

const passport = require("passport");
const passportLocal = require("passport-local")
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static("../client/dist"));

const validateUser = async (username, password, done) => {
    let user = await client.query("SELECT * FROM reddit_user WHERE email=$1;", [username])
    user = user['rows'][0];
    if (!user) {
        done(null, null);
    }
    else if (username === user.email && password === user.password) {
        done(null, {
            id: user.id,
            username: user.email,
            password: user.password
        });
    } else {
        done(null, null);
    }
}

passport.use(new passportLocal.Strategy(validateUser));
passport.deserializeUser(async (id, done) => {
    user = await client.query("SELECT * FROM reddit_user WHERE id=$1;", [id]);
    user = user["rows"][0];
    done(null, {
        id: user.id,
        username: user.email,
        password: user.password
    });
});
passport.serializeUser((user, done) => {
    done(null, user.id);
})

//Sockets

io.sockets.on("connection", (socket) => {
    socket.on('deleteComment', (comment) => {
        client.query('DELETE FROM comment WHERE id=$1 RETURNING id,content,user_id,post_id', [comment.comment_id])
            .then(() => {
                io.emit("deletedComment", comment)
            })
    })
    socket.on('deletePost', (post) => {
        //console.log(post.post_id);
        client.query('DELETE FROM post_vote WHERE post_id=$1', [post.post_id])
            .then(() => {
                client.query('DELETE FROM comment WHERE post_id=$1', [post.post_id])
                    .then(() => {
                        client.query('DELETE FROM post WHERE id=$1', [post.post_id])
                            .then(() => {
                                io.emit("deletedPost", post.post_id)
                            })
                    })

            })
    })
    socket.on('addComment', (comment) => {
        //console.log(comment)
        // Add this comment to database client query
        // in then return what I wrote below
        client.query('INSERT INTO comment (content,user_id,post_id) VALUES ($1,$2,$3) RETURNING id,content,user_id,post_id', [comment["content"], comment["user_id"], comment["post_id"]])
            .then((data) => {
                //console.log(data["rows"]);
                io.emit("addedComment", data.rows[0])
            })
            .catch(() => {
                //do zrobienia
            })
    })
    // socket.on('vote', (pack)=>{
    //     console.log("start");
    //     client.query("SELECT vote FROM post_vote WHERE user_id=$1 AND post_id=$2",
    //     [pack["user_id"], pack["post_id"]])
    //     .then((vote) => {
    //         console.log(vote)
    //         let empty = 0;
    //         if (vote["rows"].length === 0) {
    //             empty = 0;
    //         }
    //         else {
    //             empty = 1;
    //         }
    //         if (empty === 1) {
    //             if (vote["rows"][0]["vote"] === 1 && pack["voteValue"] === 1) {
                    
    //                 client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                 .then((data)=>{
                       
    //                     io.emit("sumVotes", data.rows[0]["sum"])
    //                 })
    //             }
    //             else if (vote["rows"][0]["vote"] === 1 && pack["voteValue"] === -1) {
                   
    //                 client.query("UPDATE post_vote SET vote=$1 WHERE user_id=$2 AND post_id=$3",
    //                     [pack["voteValue"], pack["user_id"], pack["post_id"]])
    //                     .then(() => {
                            
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
                              
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
        
    //                     })
    //                     .catch(() => {
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
                                
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //                     })
    //             }
    //             else if (vote["rows"][0]["vote"] === -1 && pack["voteValue"] === 1) {
                    
    //                 client.query("UPDATE post_vote SET vote=$1 WHERE user_id=$2 AND post_id=$3",
    //                     [pack["voteValue"], pack["user_id"], pack["post_id"]])
    //                     .then(() => {
                            
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
                               
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })

    //                     })
    //                     .catch(() => {
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //                     })
    //             }
    //             else if (vote["rows"][0]["vote"] === -1 && pack["voteValue"] === -1) {
                    
    //                 client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //             }
    //         }
    //         else {
    //             console.log(pack["voteValue"])
    //             if (pack["voteValue"] === 1) {
                   
    //                 client.query("INSERT INTO post_vote (vote, user_id, post_id) VALUES ($1,$2,$3)",
    //                     [pack["voteValue"], pack["user_id"], pack["post_id"]])
    //                     .then(() => {
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //                     })
    //                     .catch((err) => {
    //                         console.log(err)
    //                     })
    //             }
    //             else if (pack["voteValue"] === -1) {
                   
    //                 client.query("INSERT INTO post_vote (vote, user_id, post_id) VALUES ($1,$2,$3)",
    //                     [pack["voteValue"], pack["user_id"], pack["post_id"]])
    //                     .then(() => {
                            
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })

    //                     })
    //                     .catch(() => {
    //                         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //                     })
    //             }
    //         }
    //     })
    //     .catch(() => {
    //         client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1", [pack["post_id"]])
    //                         .then((data)=>{
    //                             io.emit("sumVotes", data.rows[0]["sum"])
    //                         })
    //     })
    // })
})





app.get("/user", async (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT * FROM reddit_user;')
        .then((data) => {
            return res.send(data.rows);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.get("/userid", async (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT id FROM reddit_user WHERE id=$1', [req.user.id])
        .then((data) => {
            return res.send(data.rows);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});



app.get("/user/:id", async (req, res) => {
    client.query('SELECT * FROM reddit_user WHERE id=$1;', [req.params.id])
        .then((data) => {
            return res.send(data.rows);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});
app.get("/lol", async (req, res) => {
    client.query('SELECT * FROM reddit_user')
        .then((data) => {
            return res.send(data.rows);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});
app.post("/login", (req, res, next) => {
    passport.authenticate("local", (err, user) => {
        if (err) {
            res.status(500);
            res.send({ 'msg': 'Something went wrong' });
        };
        if (!user) {
            res.status(401);
            res.send({ 'msg': 'Unauthorized' });
        }
        else {
            req.logIn(user, (err) => {
                if (err) throw err;
                res.send({ 'msg': "Successfully Authenticated" });
            });
        }
    })(req, res, next);
});

app.get("/logout", (req, res) => {
    req.logOut();
    res.send("Successfully logged out");
});

app.post("/register", (req, res) => {
    client.query('SELECT * FROM reddit_user WHERE email=$1',
        [req.body.email])
        .then((value) => {
            if (value["rows"].length === 0) {
                client.query('INSERT INTO reddit_user (nickname, password,email) VALUES ($1,$2,$3) RETURNING id,nickname,password,email',
                    [req.body.nickname, req.body.password, req.body.email])
                    .then(() => {
                        return res.send({ "response": "Registered successfully" })
                    })
                    .catch(() => {
                        return res.send({ "error": "Something went wrong" })
                    })
            }
            else {
                return res.send({ "response": "User with the same e-mail already exists" })

            }


        })

});

app.post("/change", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT * FROM reddit_user WHERE id=$1 AND password=$2',
        [req.user.id, req.body.password])
        .then((response) => {
            if (response["rows"].length) {
                client.query('UPDATE reddit_user SET password=$1 WHERE id=$2',
                    [req.body.password1, req.user.id])
                return res.send({ "response": "ok" })
            }
            else {
                return res.send({ "response": "nie ok" })
            }
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.get("/auth", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    else {
        return res.send({ "response": "ok" })
    }
});

app.get("/subreddit", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT * FROM subreddit;')
        .then((data) => {
            return res.send(data.rows);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.get("/subreddit/:name", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT id FROM subreddit WHERE name=$1', [req.params.name])
        .then((data) => {
            if (data["rows"].length === 0) {
                return res.send({ "Error": "Subreddit doesn't exist" })
            }
            else {
                client.query('SELECT * FROM post WHERE subreddit_id=$1', [data["rows"][0]["id"]])
                    .then((data2) => {
                        return res.send(data2.rows);
                    })
                    .catch(() => {
                        return res.send({ "error": "Something went wrong" })
                    })
            }
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.get("/subreddit/:name/descr", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT description FROM subreddit WHERE name=$1', [req.params.name])
        .then((data) => {
            if (data["rows"].length === 0) {
                return res.send({ "Error": "Subreddit doesn't exist" })
            }
            else {
                return res.send(data["rows"]);
            }
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.patch("/subreddit/:name/editdescr", (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('UPDATE subreddit SET description=$1 WHERE name=$2', [req.body.description, req.params.name])
        .then(() => {
            return res.send({ "response": "ok" })
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});



const multer = require('multer');
const upload = multer();
const fs = require("fs");
app.post('/subreddit/:name/addPost', upload.single('file'), (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    //console.log("1");
    if (req.file !== 'null' && req.file !== null && req.file !== undefined) {
        fs.writeFile(`../client/public/uploads/${req.file.originalname}`, req.file.buffer, function (err) {
            if (err) return console.log(err);
        });
    }
    //console.log("2");
    if (req.body.content === 'null') {
        req.body.content = null;
    }
    if (req.body.video_url === 'null') {
        req.body.video_url = null;
    }
    //console.log("3");
    client.query("SELECT id FROM subreddit WHERE name=$1", [req.params.name])
        .then((values) => {
            //console.log("4")
            if (req.file.originalname) {
                //console.log("5")
                client.query("INSERT INTO post (title,content,image_path,video_url,creation_date,subreddit_id,user_id) VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING id,title,content,image_path,video_url,creation_date,subreddit_id,user_id",
                    [req.body.title, req.body.content, `uploads/${req.file.originalname}`, req.body.video_url, new Date().toISOString(), values["rows"][0]["id"], req.user.id])
                    .then((values_from_insert) => {
                        return res.send(values_from_insert["rows"]);
                    })
                    .catch(() => {
                        return res.send({ "error": "Something went wrong" })
                    })
            }
            else {
                //console.log("6")
                client.query("INSERT INTO post (title,content,image_path,video_url,creation_date,subreddit_id,user_id) VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING id,title,content,image_path,video_url,creation_date,subreddit_id,user_id",
                    [req.body.title, req.body.content, "", req.body.video_url, new Date().toISOString(), values["rows"][0]["id"], req.user.id])
                    .then((values_from_insert) => {
                        return res.send(values_from_insert["rows"]);
                    })
                    .catch(() => {
                        return res.send({ "error": "Something went wrong" })
                    })
            }
        }
        )
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
}
);


app.post('/subreddit/addSubreddit', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query('SELECT * FROM subreddit WHERE name=$1',
        [req.body.name])
        .then((value) => {
            if (value["rows"].length === 0) {
                client.query("INSERT INTO subreddit (name,description) VALUES ($1,$2) RETURNING id,name,description",
                    [req.body.name, req.body.description])
                    .then((values_from_insert) => {
                        client.query("INSERT INTO subreddit_moderator (user_id,subreddit_id) VALUES ($1,$2) RETURNING id,user_id,subreddit_id",
                            [req.user.id, values_from_insert["rows"][0]["id"]])
                            .then(() => {
                                return res.send(values_from_insert["rows"]);
                            })
                            .catch(() => {
                                return res.send({ "error": "Something went wrong" })
                            })

                    })
                    .catch(() => {
                        return res.send({ "error": "Something went wrong" })
                    })

            }
            else {
                return res.send({ "response": "Subreddit already exists" })
            }

        })


});

app.get('/subreddit/mod/:name', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT * FROM subreddit WHERE name=$1",
        [req.params.name])
        .then((id) => {
            client.query("SELECT * FROM subreddit_moderator WHERE user_id=$1 AND subreddit_id=$2",
                [req.user.id, id["rows"][0]["id"]])
                .then((value) => {
                    if (value["rows"].length !== 0) {
                        return res.send({ "ifModerator": true })
                    }
                    else {
                        return res.send({ "ifModerator": false })
                    }
                })
                .catch(() => {
                    return res.send({ "error": "Something went wrong" })
                })
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});

app.get('/subreddit/:name/subscription', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT id FROM subreddit WHERE name=$1",
        [req.params.name])
        .then((id) => {
            client.query("SELECT * FROM subreddit_user WHERE user_id=$1 AND subreddit_id=$2",
                [req.user.id, id["rows"][0]["id"]])
                .then((value) => {
                    if (value["rows"].length !== 0) {
                        return res.send({ "ifJoined": true })
                    }
                    else {
                        return res.send({ "ifJoined": false })
                    }
                })
                .catch(() => {
                    return res.send({ "error": "Something went wrong" })
                })
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })

});

app.post('/subreddit/:name/subscription', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT id FROM subreddit WHERE name=$1",
        [req.params.name])
        .then((id) => {
            client.query("SELECT * FROM subreddit_user WHERE user_id=$1 AND subreddit_id=$2",
                [req.user.id, id["rows"][0]["id"]])
                .then((value) => {
                    console.log(value)
                    if (value["rows"].length !== 0) {
                        if (req.body.ifJoined) {
                            return res.send({ "error": "Cannot join joined user" })
                        }
                        else {
                            client.query("DELETE FROM subreddit_user WHERE user_id=$1 AND subreddit_id=$2", [req.user.id, id["rows"][0]["id"]])
                                .then(() => {
                                    return res.send({ "response": "User unjoined" })
                                })
                                .catch(() => {
                                    return res.send({ "error": "Something went wrong" })
                                })
                        }
                    }
                    else {
                        if (req.body.ifJoined) {
                            client.query("INSERT INTO subreddit_user (user_id, subreddit_id) VALUES ($1,$2)", [req.user.id, id["rows"][0]["id"]])
                                .then(() => {
                                    return res.send({ "response": "User joined" })
                                })
                                .catch(() => {
                                    return res.send({ "error": "Something went wrong" })
                                })
                        }
                        else {
                            return res.send({ "error": "Cannot unjoin unjoined user" })
                        }

                    }
                })
                .catch(() => {
                    return res.send({ "error": "Something went wrong" })
                })
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })

});

app.post('/subreddit/:name/p/votes', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT SUM(vote) FROM post_vote WHERE post_id=$1",
        [req.body.post_id])
        .then((sum) => {
            client.query("SELECT * FROM post_vote WHERE user_id=$1 AND post_id=$2",
                [req.user.id, req.body.post_id])
                .then((value) => {
                    return res.send({ sumVotes: sum["rows"], userVotes: value["rows"] })
                })
                .catch(() => {
                    return res.send({ "error": "Something went wrong" })
                })
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })

});

app.post('/subreddit/:name/p/voting', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT vote FROM post_vote WHERE user_id=$1 AND post_id=$2",
        [req.user.id, req.body.post_id])
        .then((vote) => {
            let empty = 0;
            if (vote["rows"].length === 0) {
                empty = 0;
            }
            else {
                empty = 1;
            }
            if (empty === 1) {
                if (vote["rows"][0]["vote"] === 1 && req.body.voteValue === 1) {
                    return res.send({ "error": "Cannot vote yes twice" })
                }
                else if (vote["rows"][0]["vote"] === 1 && req.body.voteValue === -1) {
                    client.query("UPDATE post_vote SET vote=$1 WHERE user_id=$2 AND post_id=$3",
                        [req.body.voteValue, req.user.id, req.body.post_id])
                        .then(() => {
                            return res.send({ "response": "Vote -1 added" });
                        })
                        .catch(() => {
                            return res.send({ "error": "Something went wrong" })
                        })
                }
                else if (vote["rows"][0]["vote"] === -1 && req.body.voteValue === 1) {
                    client.query("UPDATE post_vote SET vote=$1 WHERE user_id=$2 AND post_id=$3",
                        [req.body.voteValue, req.user.id, req.body.post_id])
                        .then(() => {
                            return res.send({ "response": "Vote 1 added" });

                        })
                        .catch(() => {
                            return res.send({ "error": "Something went wrong" })
                        })
                }
                else if (vote["rows"][0]["vote"] === -1 && req.body.voteValue === -1) {
                    return res.send({ "error": "Cannot vote 'no' twice" })
                }
            }
            else {
                if (req.body.voteValue === 1) {
                    client.query("INSERT INTO post_vote (vote, user_id, post_id) VALUES ($1,$2,$3)",
                        [req.body.voteValue, req.user.id, req.body.post_id])
                        .then(() => {
                            return res.send({ "response": "Vote 1 added(0)" });

                        })
                        .catch((err) => {
                            console.log(err)
                        })
                }
                else if (req.body.voteValue === -1) {
                    client.query("INSERT INTO post_vote (vote, user_id, post_id) VALUES ($1,$2,$3)",
                        [req.body.voteValue, req.user.id, req.body.post_id])
                        .then(() => {
                            return res.send({ "response": "Vote -1 added(0)" });

                        })
                        .catch(() => {
                            return res.send({ "error": "Something went wrong" })
                        })
                }
            }
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});


app.post('/subreddit/:name/p/comments', (req, res) => {
    if (!req.isAuthenticated()) {
        res.status(401);
        return res.send({ "error": "Unauthorized" })
    }
    client.query("SELECT * FROM comment WHERE post_id=$1",
        [req.body.post_id])
        .then((values) => {
            return res.send(values["rows"]);
        })
        .catch(() => {
            return res.send({ "error": "Something went wrong" })
        })
});



require("dotenv").config();
const dbConnData = {
    host: process.env.PGHOST || "localhost",
    port: process.env.PGPORT || 6270,
    database: "postgres",
    user: "postgres",
    password: "password"
};


const { Client } = require("pg");
const client = new Client(dbConnData);
console.log("Connection parameters: ");
console.log(dbConnData);
client
    .connect()
    .then(async () => {
        console.log("Connected to PostgreSQL");
        const port = 6001;
        http.listen(port, () => {
            console.log(`API server listening at http://192.168.0.12:${port}`);
        });
    })
    .catch(err => console.error("Connection error", err.stack));
